class HumanPlayer
  def get_play
    until answer.length == 2
      p 'Please enter your coordinates as the example: #, #'
      answer = gets.chomp.split(', ').map(&:to_i)
    end
    answer
  end
end
