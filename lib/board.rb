class Board
  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    grid = []
    10.times { grid << Array.new(10, []) }
    grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end
  
  def count
    ships = 0
    @grid.each do |row|
      row.each do |pos|
        ships += 1 if pos == :s
      end
    end
    ships
  end

  def empty?(pos = nil)
    if pos.nil?
      @grid.flatten.each { |char| return false if char == :s }
      return true
    end
    @grid[pos[0]][pos[1]].nil?
  end

  def full?
    @grid.each do |row|
      row.each do |char|
        return false if char.nil?
      end
    end
    true
  end

  def place_random_ship
    raise 'FULLBRUH' if full?
    row = (0..@grid.length - 1).to_a
    col = (0..@grid[0].length - 1).to_a
    placed = false
    until placed == true
      rand_row = row.sample
      rand_col = col.sample
      if @grid[rand_row][rand_col].nil?
        @grid[rand_row][rand_col] = :s
        placed = true
        row.delete(rand_row)
        col.delete(rand_col)
      end
    end
  end

  def won?
    clone = @grid.clone.flatten
    clone.any? { |char| char == :s } == true ? false : true
  end

end
